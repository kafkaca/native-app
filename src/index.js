import React, { Component, PropTypes } from 'react'
import { Scene, Router} from 'react-native-router-flux'
import PageOne from './components/PageOne'
import PageTwo from './components/PageTwo'
import { connect, Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { View, Text } from 'react-native';
import Routes from './containers/Router'
import Donemec from './containers/donemec'
import configureStore from './store/configureStore'
const store = configureStore()

  export default class ReduxBase extends Component {
    constructor(props) {
      super(props);
    console.log("INDEX PROPS : ", this.props)
      this.state = {};
    }

    
    
    render() {
        return (
           <Provider store={store}>
           <Routes />
           </Provider>
           );
       }
   }