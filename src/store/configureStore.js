import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootReducer from '../reducers'
import promise from './promise'
import array from './array'

export default function configureStore(initialState) {
	const middleware = [thunk, promise, array, logger]
	const enhancer = compose(
		applyMiddleware(...middleware)
		)
	return createStore(rootReducer, initialState, enhancer)
}