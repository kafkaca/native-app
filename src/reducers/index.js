import { combineReducers } from 'redux';
import Example from './example';
import User from './user';
import Routes from './routes';
import nav, * as fromNav from './navReducer'
import home, * as fromHome from './homeReducer'

export default combineReducers({
  Example,
  User,
  Routes,
  nav,
  home
});

export const getNav = ({nav}) => nav
export const getHome = ({home}) => home
