import React from 'react';
import { Scene, Router, ActionConst, Actions } from 'react-native-router-flux';
import PageOne from '../components/PageOne';
import PageTwo from '../components/PageTwo';
import Home from '../components/Home'
import DemoText from '../components/DemoText'
import DemoOtherText from '../components/DemoOtherText'
const RouterWithRedux = connect()(Router);
import { connect, Provider } from 'react-redux';


const Scenes = Actions.create(
                <Scene key="root">
                    <Scene
                        key="DemoText"
                        component={DemoText}
                        title="DemoText"
                        hideNavBar
                    />                    
                    <Scene
                        key="DemoOtherText"
                        component={DemoOtherText}
                        title="DemoOtherText"
                        hideNavBar
                    />
                     <Scene
                        key="Home"
                        component={Home}
                        title="Demo Home"
                        hideNavBar
                        initial
                    />
                </Scene>,
);

export default () => <RouterWithRedux scenes={Scenes} />