import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Subtitle, View} from 'native-base';

export default class HeaderIconTextExample extends Component {
  render() {
    return (
      <Header>
        <Button>
          <Text>sol</Text>
        </Button>
        <Body>
        <Title style={{color:'white', alignSelf:'center', margin:5, marginLeft:26}}>TestProject</Title>
        </Body>
          <Button>
             <Text>sag</Text>
          </Button>
      </Header>
    );
  }
}