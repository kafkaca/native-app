import React, { Component} from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Button, Content, Grid, Row, Text } from 'native-base';

class PageTwo extends Component {
    constructor(props) {
      super(props);
      this.state = {}
      console.log(props)
    }

    render() {
        return (
           <Container>
                <Content justifyContent="center" alignItems="center" style={{ padding: 4, }}>
                    <Grid style={{ alignItems: 'center' }}>
                        <Row>
                            <Button onPress={Actions.donemec}>
                                <Text>
                                    Donemec
                                </Text>
                            </Button>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}



export default PageTwo;
