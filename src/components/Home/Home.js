import React from 'react'
import {
  View,
  Text,
} from 'react-native'
import {Actions} from 'react-native-router-flux'
import CardImage from './cardImage'
export default (props) => {
  const {count, scene, incrementCount, decrementCount, incrementCountThunk, handleCard} = props

    console.log('HOME/HOME', props)
    console.log('Actions.currentScene', Actions)

  return (
    <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>You are currently on {scene && scene.sceneKey}</Text>
      <Text>Count: {count}</Text>
      <Text onPress={()=>incrementCount()}>Increment Count</Text>
      <Text onPress={()=>incrementCountThunk()}>Increment Count Thunk</Text>
      <Text onPress={()=>decrementCount()}>Decrement Count</Text>

      <Text onPress={()=>handleCard("bu bir sayfa yonlendi")}>Push New Card Scene</Text>
<CardImage card_name={count} />

    </View>
  )
}
